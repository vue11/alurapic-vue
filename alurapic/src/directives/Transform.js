import Vue from 'vue';

Vue.directive('meu-transform', {
    bind(element, binding, vnode) {
        console.log('Diretiva associada!');
        console.log(element);

        let current = 0;

        element.addEventListener('dblclick', function () {
            let incremento = binding.value || 90;
            let efeito;

            if (!binding.arg || binding.arg == 'rotate') {
                console.log('1');
                if (binding.modifiers.reverse) {
                    current -= incremento;
                } else {
                    current += incremento;
                }
                efeito = `rotate(${current}deg)`;

            } else if (binding.arg == 'scale') {
                efeito = `scale(${incremento})`;
            }

            element.style.transform = efeito;

            if (binding.modifiers.animacao) { // O valor de binding.modifiers.animacao é sempre true quando o modificador estiver presente na diretiva.
                element.style.transition = "transform 0.5s";
            }
        });
    }
});