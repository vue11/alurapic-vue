import Vue from 'vue'
import VueResource from 'vue-resource'

import VueRouter from 'vue-router'

import App from './App.vue'
import { routes } from './routes.js' // importando as rotas
import './directives/Transform' // importando minha diretiva

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// importando o VeeValidate 
import VeeValidate from 'vee-validate';
import msg from './pt_BR';


// registrando os plugins 
Vue.use(VeeValidate, {
  locale: 'pt_BR',
  dictionary: {
    pt_BR: {
      messages: msg
    }
  }
});

Vue.use(VueResource); // Indico que alguns recursos do VueResource estarão disponivel em todos os componentes Single File Application 
Vue.use(VueRouter);
Vue.use(BootstrapVue);

Vue.http.options.root = 'http://localhost:3000' // O valor passado para a propriedade root será a raiz de toda requisição, assim centralizo o dominio da API 
Vue.http.interceptors.push((req, next) => { // adiciono um simples interceptador que adiciona um header na requisição antes de ser enviada e que exibe os dados vindos do servidor de qualquer resposta

  // é possível colocar informações no header antes do envio da requisição
  //req.headers.set('Authorization', 'informação de segurança aqui');
  console.log('Lidando com o request');

  next(res => {
    console.log('Lidando com a resposta')
    // é possível acessar os dados da reposta e realizar transformações antes
    console.log(res.body);
  });

});

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  el: '#app', // elemento que corresponde a um componente que vai ser renderizado no index.html
  render: h => h(App),
  router
});






