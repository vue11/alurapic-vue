import Home from './components/home/Home.vue';
import Cadastro from './components/cadastro/Cadastro.vue';

export const routes = [

    // path: endereço, name: para usar no router link, component: componente carregado, titulo: para usar no router link, menu: indico se faz parte do menu 

    { path: '/', name: 'home', component: Home, titulo: 'Home', menu: true }, // localhost:3000/#/
    { path: '/cadastro', name: 'cadastro', component: Cadastro, titulo: 'Cadastro', menu: true }, // localhost:3000/#/cadastro
    { path: '/cadastro/:id', name: 'altera', component: Cadastro, titulo: 'Cadastro', menu: false }, // localhost:3000/#/cadastro
    { path: '*', component: Home, menu: false } // * Qualquer coisa, o componente carregado será Home 
];


