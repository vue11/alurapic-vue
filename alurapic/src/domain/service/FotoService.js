export default class FotoService {

    constructor(resource) {
        this._resource = resource('v1/fotos{/id}');
    }

    lista() {
        return this._resource.query() // query só leva em consideração v1/fotos, excluindo o parâmetro id.
            .then(response => response.json(), // Uso o then para obter o resultado da promisse e converto em JSON para pegar somente os dados retornados
            error => {
                console.log(error);
                throw new Error("Pedimos desculpas, não estamos conseguindo recuperar as informações no momento. Por favor, tente novamente mais tarde.");
            }); 
    }

    cadastra(foto) {
        if (foto._id) {
            return this._resource.update({ id: foto._id }, foto);
        } else {
            return this._resource.save(foto);
        }

    }

    remove(id) {
        return this._resource.delete({ id })
        .then(null, error => {
            console.log(error);
            throw new Error("Não foi possível remover a foto!");
        } );
    }

    busca(id) {
        return this._resource.get({ id })
            .then(response => response.json());
    }

}